class Cell:
    def __init__(self):
        self._symbol = None

    @property
    def symbol(self):
        return ' ' if self._symbol is None else self._symbol

    @symbol.setter
    def symbol(self, symbol):
        self._symbol = symbol


class Board:
    def __init__(self, size=3):
        self.grid = [[Cell() for _ in range(size)] for _ in range(size)]

    def _get_item(self, i, j):
        return self.grid[i][j].symbol

    def _check_grid(self, symbol, i, j):
        # Проверка по горизонтали
        horizontal = all(cell.symbol == symbol for cell in self.grid[i])
        # Проверка по вертикали
        vertical = all(self.grid[k][j].symbol == symbol for k in range(self.size))
        # Проверка по основной диагонали
        diagonal_main = all(self.grid[k][k].symbol == symbol for k in range(self.size))
        # Проверка по дополнительной диагонали
        diagonal_aux = all(self.grid[self.size - k - 1][k].symbol == symbol for k in range(self.size))

        return horizontal or vertical or diagonal_main or diagonal_aux

    def _check_occupied(self, i, j):
        if self.grid[i - 1][j - 1].symbol != ' ':
            raise ValueError('Клетка занята')

    def plant(self, symbol, i, j):
        self._check_occupied(i, j)
        self.grid[i - 1][j - 1].symbol = symbol
        return self._check_grid(symbol, i - 1, j - 1)

    def __repr__(self):
        printed_grid = ''

        printed_grid += '-' * (self.size * 2 + 1) + '\n'
        for i in range(self.size):
            for j in range(self.size):
                printed_grid += f'|{self._get_item(i, j)}'
            printed_grid += '|\n' + '-' * (self.size * 2 + 1) + '\n'

        return printed_grid

    @property
    def size(self):
        return len(self.grid)
