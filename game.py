import config

from board import Board
from user import User


def build_users(symbols):
    return [User(symbol) for symbol in symbols]


def validate_move(raw_move, board_size):
    moves = raw_move.split(' ')

    try:
        x = int(moves[0])
        y = int(moves[1])
    except ValueError:
        return None

    if x > board_size or y > board_size or x < 1 or y < 1:
        return None
    else:
        return x, y


def make_move(symbol, i, j, board):
    pass


def play(players, board):
    while True:
        for player in players:
            print(board)
            print(f'Очередь игрока {player.symbol}!')

            coords = None
            occupied = True
            while not coords or occupied:
                print('x y >', end='')
                move = input()
                coords = validate_move(move, board.size)
                if coords:
                    try:
                        victory = board.plant(player.symbol, coords[0], coords[1])
                        occupied = False
                    except ValueError:
                        pass

            if victory:
                print(board)
                return player.symbol


def main(player_symbols, board_size):
    users = build_users(player_symbols)
    print('Добро пожаловать в крестики-нолики!')
    print('Счет ячеек начинается с 1, считая с левого верхнего угла.')
    print('Нажмите Enter, чтобы начать новую игру.')

    while True:
        key = input()
        if key == 'q':
            break

        game_board = Board(size=board_size)
        winner = play(users, game_board)
        print(f'Победил игрок {winner}!')
        print('Нажмите Enter, чтобы играть еще раз или введите q для выхода.')

    print('Спасибо за игру!')


if __name__ == '__main__':
    main(config.PLAYERS, config.BOARD_SIZE)
