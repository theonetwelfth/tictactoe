import game
import pytest

from board import Board


def test_smoke():
    board = Board()
    board.plant('X', 1, 1)
    board.plant('Y', 2, 2)
    board.plant('X', 3, 3)
    assert board.grid[0][0].symbol == 'X'
    assert board.grid[1][1].symbol == 'Y'
    assert board.grid[2][2].symbol == 'X'


def test_duplicate():
    with pytest.raises(ValueError):
        board = Board()
        board.plant('X', 1, 1)
        board.plant('Y', 1, 1)


def test_horizontal():
    board = Board()
    board.plant('X', 1, 1)
    board.plant('X', 1, 2)
    victory = board.plant('X', 1, 3)
    assert victory


def test_vertical():
    board = Board()
    board.plant('X', 1, 1)
    board.plant('X', 2, 1)
    victory = board.plant('X', 3, 1)
    assert victory


def test_diagonal_main():
    board = Board()
    board.plant('X', 1, 1)
    board.plant('X', 2, 2)
    victory = board.plant('X', 3, 3)
    assert victory


def test_diagonal_aux():
    board = Board()
    board.plant('X', 3, 1)
    board.plant('X', 2, 2)
    victory = board.plant('X', 1, 3)
    assert victory
